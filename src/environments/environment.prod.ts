// Define environment variables for production mode
export const environment = {
  production: true,
  apiUsers: "own-api-url-for-user",
  apiKey: "api-key",
  apiPokemon: "https://pokeapi.co/api/v2/pokemon?limit=151"

};
