// Define environment variables for dev mode
export const environment = {
    production: false,
    apiUsers: "own-api-url-for-user",
    apiKey: "api-key",
    apiPokemon: "https://pokeapi.co/api/v2/pokemon?limit=151"
}
