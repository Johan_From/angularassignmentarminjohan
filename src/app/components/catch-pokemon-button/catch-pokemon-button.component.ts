import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CatchService } from 'src/app/services/catch.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {


  // Define booleans
  public loading: boolean = false;
  public caught: boolean = false
  public battle: boolean = false;
  public addedToBattle: boolean = false;
  public showButton: boolean = false;

  // Define pokemonId and set to empty string
  @Input() pokemonId: string = "";

  constructor(private readonly userService: UserService, private readonly catchService: CatchService,
    private readonly pokemonService: PokemonService,
    private readonly route: ActivatedRoute ) { }


  ngOnInit(): void {
    //checks if pokemon is caught and set variable accordingly
    this.caught = this.userService.isCaught(this.pokemonId)
    
    // If location url is /Trainer set this battle 
    // to true in order to see the battle link
    if(window.location.pathname === "/trainer"){
      this.battle = true;
    }
    
  }

  // Get pokemons array from pokemonservice
  get pokemons(): Pokemon[]{
    return this.pokemonService.pokemons
  }

  // Get pokemons for battle
  get pokemonsArray(): Pokemon[]{
    return this.pokemonService.array;
  }

  //Remove pokemon from battle array by calling removePokemonFromFight()
  // set addedToBattle boolean to false
removeFromBattle(){
  const pokemon: Pokemon | undefined = this.battlePokemonById(this.pokemonId);
  
    if(confirm(`Do you want to remove ${pokemon?.name}?`) == true){ 
      alert(`${pokemon?.name} was removed from the fight rooster!`) 
      this.removePokemonFromFight(this.pokemonId)
      this.addedToBattle = false;
      
      return
    }
    else{
      return;
    }
  
}
  
  addToFight(): void{
    let id: string = this.pokemonId
    
    // Get the pokemon with id
    const pokemon: Pokemon | undefined = this.battlePokemonById(id);
   
    // Add to fight rooster if there is not already two pokemons in array
    if(this.pokemonService.array.length != 2){
      alert(`${pokemon?.name} was added to the fight rooster!`)
      this.pokemonService.array.push(pokemon)
      this.addedToBattle = true;
    }
    else{
      return;
    }

  
  }

  onCaughtClick(): void {
    this.loading = true;
    // Catch a new pokemon
    this.catchService.catchNewPokemon(this.pokemonId)
    .subscribe({
      next: (user: User) => {
        if(this.pokemonId == null){
          // If pokemonid is null then alert user
          this.loading = false;
          alert("Could not release pokemon!")
        }
        else{
          // Call the isCaught method is userService
          this.loading = false;
          this.caught = this.userService.isCaught(this.pokemonId)
          
        }
      },
      error: (error: HttpErrorResponse) => {
        this.loading = false;
        console.log("ERROR: " + error.message);
      }
    })
  }

  // Get the battlepokemonId
  public battlePokemonById(id: string): Pokemon | undefined{
    return this.userService.user?.pokemon.find((pokemon: Pokemon) => pokemon.id === id)
  }

  // Remove pokemon from battle rooster
  public removePokemonFromFight(pokemonId: string): void {  
    const pokemon = this.pokemonService.array.find((x: Pokemon) => x.id === pokemonId); // Find pokemon
    const index = this.pokemonService.array.indexOf(pokemon); // Find the index of pokemon in battlearray
    if(index > -1){
      this.pokemonService.array.splice(index, 1) // Remove from array if conditition is met
    } 
  }

}
