import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {

  // Pokemons
  @Input() pokemon?: Pokemon;

  // Define services
  constructor(private readonly userService: UserService) { }

  // Booleans for show information about pokemon and show wins
  showInfo: boolean = false;
  showWins: boolean = false

  // Method for showing pokemon info
  showPokemonInfo(){
    this.showInfo = !this.showInfo;
  }

  // Getter for displaying the pokemon
  get displayPokemon(){
    if(this.pokemon?.id === undefined){
      return false
    } else{
      return true;
    }
  }

  ngOnInit(): void {
    // If you are in trainer page show the wins
    if(window.location.pathname === "/trainer"){
      this.showWins = true;
    }
  }

}
