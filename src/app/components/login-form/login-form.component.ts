import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  // Define isloading boolean in order to show animation while loading
  isLoading: boolean = false;

  @Output() login: EventEmitter<void> = new EventEmitter()

  //Define services in constructor
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService
    ) { }


  public loginSubmit(loginForm: NgForm): void {
    // Get username from the loginform
    const { username } = loginForm.value;

    if(username.length < 3){
      alert("Username to short!\nMust be at least 3 three characters.")
      return;
    }

    this.isLoading = true;
    
    //Call method from service and subscribe user
    // if user is undefined set loading boolean to false
    this.loginService.login(username)
    .subscribe({
      next: (user: User) => {
        if(user == undefined){
          this.isLoading = false;
        }
        this.isLoading = false;
        // Set user from service to current user
        this.userService.user = user;
        //Emit event with value from login
        this.login.emit();
      },
      error: () => {
        
      }
    })
  }
}
