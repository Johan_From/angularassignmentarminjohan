import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})

export class PokemonListComponent{
  // Pokemon list
  @Input() pokemons: Pokemon[] = []

  // Input string for filtration
  filteredString: string = "";
  
  // Pagination number
  cp: number = 1;
  constructor() { }

}
