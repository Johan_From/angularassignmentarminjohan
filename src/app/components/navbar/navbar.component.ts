import { Component, OnInit, OnChanges } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  // Define showBattleLink boolean to toggle visibility of battle link
  showBattleLink: boolean = true;

  // Get user from service
  get user(): User | undefined{
    return this.userService.user;
  }

  // Get battle pokemons array
  get pokemons(): any{
    return this.pokemonService.array
  }
  
  //Define services in constructor 
  constructor(private readonly userService: UserService,
    private readonly pokemonService: PokemonService) { }

  // If battlepokemons array is less than 1 hide battle link
  battleLink(){
    if(this.pokemons.length < 1){
      this.showBattleLink = false;
    }
  }

  // Reloads the page
  refresh(){
    return window.location.reload
  }

 // If location is /battle and a refresh occurs, redirect user to /trainer page
 // This is set in order to prevent to empty array of battlepokemons while on battlepage and reloading the page
  ngOnInit(): void {
    if(window.location.pathname === "/battle" && this.refresh() ){
        window.location.pathname = "/trainer"
    }

    
  }


}
