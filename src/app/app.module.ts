import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LandingPage } from './pages/landing/landing.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule } from '@angular/forms';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { CatchPokemonButtonComponent } from './components/catch-pokemon-button/catch-pokemon-button.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { BattlePage } from './pages/battlepage/battle.page';
import { LoadingPokeballComponent } from './components/loading-pokeball/loading-pokeball.component';
import { FilterPipe } from './pipes/filter.pipe';

// Declare all the moduels
@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    TrainerPage,
    BattlePage,
    PokemonCataloguePage,
    LoginFormComponent,
    NavbarComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    CatchPokemonButtonComponent,
    LoadingPokeballComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
