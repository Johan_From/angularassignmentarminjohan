import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  // Define router
  constructor(private readonly router: Router) { }

  // Logout user
  public logout(key: string): void{
    StorageUtil.storageDelete(key); // Delete the sessionStorage with key
    this.router.navigateByUrl("/login") // Navigate back to loginpage
    window.location.reload();
  }
}
