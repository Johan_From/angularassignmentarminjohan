import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonService } from './pokemon.service';
import { UserService } from './user.service';


// Get apiKey and ApiUsers from environment file
const {apiKey, apiUsers } = environment

@Injectable({
  providedIn: 'root'
})
export class CatchService {

  // Define HTTPclient and services in constructor
  constructor(
    private readonly http: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly userService: UserService) { }

    // Catch pokemon 
    public catchNewPokemon(pokemonId: string): Observable<User> {
      //If there is no users, throw error
      if(!this.userService.user){
        throw new Error("catchNewPokemon: There is no users!")
      }
      
      // Define user from service
      const user: User = this.userService.user;
      
      //Find pokemon by pokemon id
      const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId)
      
      // If pokemon is undefined, alert 
      if(pokemon == undefined){
        alert("There was en error while releasing the pokémon")
      }
      
      // If there is no pokemon, throw error
      if(!pokemon){
        throw new Error("catchNewPokemon: No pokemon with id: " + pokemonId)
      }

      //if pokemon is caught, release pokemon when clicked again on same button
      // else catch pokemon
      if(this.userService.isCaught(pokemonId)){
        this.userService.releasePokemon(pokemonId)
        
        const pokemon = this.pokemonService.array.find((x: Pokemon) => x.id === pokemonId)
        if(pokemon){
          const index = this.pokemonService.array.indexOf(pokemon); // Find the index of pokemon in battlearray
          if(index > -1){
            this.pokemonService.array.splice(index, 1)
          }
        }
      } else{
        this.userService.catchPokemon(pokemon)
      }
  
      // Headers for http 
      const headers = new HttpHeaders({
        'content-type': 'application/json',
        'x-api-key': apiKey!
      })
  
      // HTTP Patch with defined heders
      return this.http.patch<User>(`${apiUsers}/${user.id}`, {
        // set pokemons to user Pokemons
        pokemon: [...user.pokemon] // Already updated
      }, {
        headers
      })
      .pipe(
        tap((updatedUser: User) => {
          // Set user from service to updated user
          this.userService.user = updatedUser;
        })
      )
    } 
}
