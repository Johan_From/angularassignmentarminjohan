import { Injectable } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  // Define user
  private _user?: User;

  // Get user
  get user(): User | undefined {
    return this._user;
  }

  // Set user, also in storage
  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
    this._user = user;
  }

  // Read user from session storage
  constructor() {
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)
  }

  // Checks if pokemon is caught
  public isCaught(pokemonId: string): boolean {
    if(this._user){
      return Boolean(this._user?.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId))
    }
    return false;
  }

  // Catches pokemons by adding pokemon to list if there is a user
  public catchPokemon(pokemon: Pokemon): void {
    if(this._user){
      this._user.pokemon.push(pokemon)
    }
  }

  // Releases pokemon if there is a user, filters out pokemon by id
  public releasePokemon(pokemonId: string): void {
    if(this._user){
      this._user.pokemon = this._user.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId)
    }
  }
}
