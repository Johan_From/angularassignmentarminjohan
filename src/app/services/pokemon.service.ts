import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { UserService } from './user.service';

// Env variable
const {apiPokemon} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  // Variables for pokemon, error and loading
  public _pokemons: any[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  private _array: any[] = []


  // Getters and Setters
  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  get array(): any[] {
    return this._array
  }

  set array(pokemon: any[]) {
    this._array = pokemon
  }

  // Define Http and UserService
  constructor(private readonly http: HttpClient,
    private readonly userService: UserService) { }

    // First api call for fetching pokemons with, next api call url
  public fetchPokemons(): Observable<any>{
    if(this.pokemons.length > 0){ 
      return of()
    }

    // Return pokemons
    return this.http.get<any>(apiPokemon!)
    .pipe(
      map((response) => {
        return response;
      })
    )
  }

  // Funciton for fetching data about pokemon
  public fetchPokemon(url: string): Observable<Pokemon> {
    if(this.pokemons.length > 0){
      return of()
    }
    
    // return pokemon info
    return this.http.get<Pokemon>(url)
  }

  // Method for adding pokemons to fightlist
  public addToFightList(pokemon: Pokemon): void{
    this._array.push(pokemon)
  }

  // Get pokemon by Id
  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id) 
  }

  // get pokemonById for fighting array
  public pokemonById2(id: string): Pokemon | undefined {
    return this._array.find((pokemon: Pokemon) => pokemon.id === id) 
  }

  // Setting the winner with patching the array
  public setWinner(arrayOfPokemons: Pokemon[] | undefined ):Observable<any>{
    const {apiKey, apiUsers} = environment // Env 
    const user: User | undefined = this.userService.user; // User

    // Headers
    const headers = new HttpHeaders({
          'content-type': 'application/json',
          'x-api-key': apiKey
        })
        // return patched pokemon
        return this.http.patch<User>(`${apiUsers}/${user?.id}`, {
          pokemon: arrayOfPokemons // Already updated
        }, {
          headers
        })
        .pipe(
          tap((updatedUser: User) => {
            // Updated the user
            this.userService.user = updatedUser;
          })
        )
  }
  
}
