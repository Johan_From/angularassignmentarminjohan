import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

// Env variables
const {apiUsers, apiKey} = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Define HttpClient
  constructor(private readonly http: HttpClient) { }

  // Login method
  public login(username: string): Observable<User> {
    return this.checkUsername(username) // Check if username exists
    .pipe(
      switchMap((user: User | undefined) => {
        if(user === undefined){
          // If not exists, create one?
          if(confirm(`The user with name ${username} does not exists!\nDo you want to create the user?`) === false){
            // If not create, return user as undefined
            return of(user!);
          }
          // If want to create, create new user
          return this.createUser(username)
        }
        // login as usual
        return of(user); 
      })
    )
  }

  // Method for checking username
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
    .pipe(
      map((response: User[]) => {
        // Return the user response
        return response.pop();
      })
    )
  }

  // Method for creating user
  private createUser(username: string): Observable<User> {
    // Define user object
    const user = {
      username,
      pokemon: []
    }

    // Headers
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })

    // return new user
    return this.http.post<User>(apiUsers, user, {
      headers
    })
  }
}
