import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  // define service and router in constructor
  constructor(
    private readonly router: Router,
    private readonly userService: UserService
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    // If there is user, return true
    if(this.userService.user){
      return true;
    }
    //Else navigate back to login page, there is authguard on routes
    else {
      this.router.navigateByUrl("/login")
      return false;
    }
  }
  
}
