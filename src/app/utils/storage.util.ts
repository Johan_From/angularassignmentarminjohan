export class StorageUtil {

    // Storage save method, sets key and stringifys value
    public static storageSave <T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value))
    }

    // Reads storage, gets item by get
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);

        try {
            // If there is a stored value, parse stored value as T 
            if(storedValue){
                return JSON.parse(storedValue) as T
            }
            return undefined
        // catch error and remove item with key, return undefined
        } catch (error) {
            sessionStorage.removeItem(key)
            return undefined
        }
    }

    // Removes item from storage by key
    public static storageDelete(key: string): void {
        sessionStorage.removeItem(key)
    }

}