export class StringFormatting {
    // Sets first character to uppercase
    public static firstCaseUpper(str: string): string {
        return str.charAt(0).toUpperCase()+str.slice(1)
    }
}