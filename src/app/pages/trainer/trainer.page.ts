import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { LogoutService } from 'src/app/services/logout.service';
import { UserService } from 'src/app/services/user.service';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

  
  // Define services in constructor
  constructor(private readonly logoutService: LogoutService,
    private readonly userService: UserService,
    private readonly pokemonService: PokemonService
    ) { 
    }


  // Get user from service
  get user(): User | undefined{
    return this.userService.user
  }

  // Get catched pokemons, if user from service is true, get user pokemons
  get catchedPokemons(): Pokemon[]{
    if(this.userService.user){
      return this.userService.user.pokemon
    }
    return [];
  }

  // Logout, pass storagekey to method to clear session storage
  logout(): void {
    this.logoutService.logout(StorageKeys.User)
  }
  
}
