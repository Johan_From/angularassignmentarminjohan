import { Component, Input, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { StringFormatting } from 'src/app/utils/string-formatting.util';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  // Define service in constructor
  constructor(private readonly pokemonService: PokemonService) { }
  

  // Get pokemons array from service
  get pokemons(): Pokemon[]{
    return this.pokemonService.pokemons
  }
  

  pokemonsArray = this.pokemons
  
  // Shufffles pokemons randomly, for display purposes
  shuffleArray(arr: any): [] {
    let curId: number = arr.length;
    // There remain elements to shuffle
    while (0 !== curId) {
      // Pick a remaining element
      let randId = Math.floor(Math.random() * curId);
      curId -= 1;
      // Swap it with the current element.
      let tmp = arr[curId];
      arr[curId] = arr[randId];
      arr[randId] = tmp;
    }
    return arr;
  }

  // Get pokemons from service
  getPokemons(): void {
    this.pokemonService.fetchPokemons().subscribe((response) => {      
      let pokemons = response.results;
      pokemons.forEach((pokemon:any) => {        
        this.pokemonService.fetchPokemon(pokemon.url)
        .pipe(
          map((result:any) => {  
            // Set first letter to uppercase             
            let upperName = StringFormatting.firstCaseUpper(result.species.name)
            let upperType = StringFormatting.firstCaseUpper(result.types[0].type.name)
            let abilityOne = StringFormatting.firstCaseUpper(result.abilities[0].ability.name)
            let abilityTwo = StringFormatting.firstCaseUpper(result.abilities[1].ability.name)

            // Define pokemon object
            let object: Pokemon = {
              health: result.stats[0].base_stat, 
              isAlive: true,
              attack: 0,
              id: result.id,
              name: upperName,
              image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${result.id}.png`,
              baseExperience: result.base_experience,
              element: upperType,
              ability1: abilityOne,
              ability2: abilityTwo,
              wins: 0
            }
            return object;
          })
        ).subscribe({
          next: (response) => {
            // Push pokemons to pokemons array
            this.pokemons.push(response);
          },
          error:(error) => {
            //Console log error 
            console.log(error);
          }
        })
      })
    })
  }

  ngOnInit(): void {
    // Call get pokemons on init 
    this.getPokemons()
  }

  
}
