import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage{

  //Define router
  constructor(private readonly router: Router) { }

  // Set method on app-login-form to navigate to /pokemons page
  handleLogin(): void {
    this.router.navigateByUrl("/pokemons")
  }

}
