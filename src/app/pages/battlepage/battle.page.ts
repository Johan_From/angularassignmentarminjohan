import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-battle',
  templateUrl: 'battle.page.html',
  styleUrls: ['./battle.page.css']
})
export class BattlePage{
  
  // Define services in constructor
  constructor(private readonly pokemonService: PokemonService,
    private readonly http: HttpClient,
    private readonly userService: UserService) { 
    }

    // User object
  user?: User = this.userService.user;

  // Getter for pokemons array for batte
  get pokemons(): Pokemon[]{
    return this.pokemonService.array;
  }
  // Boolean for battlebutton, not possible to click button if false
  battleButtonDisabled: boolean = false;

  // Define variables for pokemons, turns, battletext and animation
  pokemon1 = this.pokemons[0];
  pokemon2 = this.pokemons[1];
  turn: number = 0;
  battleText: string = "";
  animateImg: string = "animate-bounce";
  
  // Health for the two pokemons
  pokemon1health = this.pokemons[0].health;
  pokemon2health = this.pokemons[1].health;
  
  // Define pokemon object 
  pk1: any = this.pokemon1 = {
    health: this.pokemons[0].health, 
    isAlive: true,
    attack: 0,
    id: this.pokemons[0].id,
    name: this.pokemons[0].name,
    image: this.pokemons[0].image,
    baseExperience: this.pokemons[0].baseExperience, 
    element: this.pokemons[0].element,
    ability1: this.pokemons[0].ability1,
    ability2: this.pokemons[0].ability2,
    wins: this.pokemons[0].wins
  }
  
  // Define pokemon object 
  pk2: any = this.pokemon2 = {
    health: this.pokemons[1].health, 
    isAlive: true,
    attack: 0,
    id: this.pokemons[1].id,
    name: this.pokemons[1].name,
    image: this.pokemons[1].image,
    baseExperience: this.pokemons[1].baseExperience, 
    element: this.pokemons[1].element,
    ability1: this.pokemons[1].ability1,
    ability2: this.pokemons[1].ability2,
    wins: this.pokemons[1].wins
  }
   
  // Method for fighting
  public fight(): void{
      this.battleButtonDisabled = true; // Cant press the battle once the function is active
        if(this.turn % 2 === 0){ // Check the turn with modulus, toogle between turns
          
          // Alter the data for each turn 
          this.pk2.attack = Math.floor(Math.random()*30)
          this.pk1.health -= this.pk2.attack
          this.turn++;
          let attacksArray = [this.pk2.ability1, this.pk2.ability2]
          let randomAttack = attacksArray[Math.floor(Math.random()* attacksArray.length)];
          this.battleText = `${this.pk2.name} uses ${randomAttack} 
                            and deals ${this.pk2.attack} damage!`
          if(this.pk1.health <= 0){
            // When pokemon 1 has 0 health
            this.pk1.isAlive = false;
            this.pk1.health = 0;
            this.checkEndGame();
            this.pokemons.length = 0;
            return;
          }
          } else{
            // Alter the data for each turn 
            this.pk1.attack = Math.floor(Math.random()*30)
            this.pk2.health -= this.pk1.attack
            this.turn++
            let attacksArray = [this.pk1.ability1, this.pk1.ability2]
            let randomAttack = attacksArray[Math.floor(Math.random()* attacksArray.length)];
            this.battleText = `${this.pk1.name} uses ${randomAttack} 
                            and deals ${this.pk1.attack} damage!`
            
            if(this.pk2.health <= 0){
              // When pokemon 1 has 0 health
              this.pk2.isAlive = false;
              this.pk2.health = 0;
              this.checkEndGame();
              this.pokemons.length = 0;
              return;
            }
          }
        
        // Timeout for each turn 
        setTimeout(() => {
          this.fight();
        }, 1500);

  }

  // Method for checking the winner
  public checkEndGame(): void{
    if(this.pk1.isAlive){ // If pokemon 1 is alive
      this.battleText = `${this.pk1.name} win`
      let pokemon = this.userService.user?.pokemon.find(x => x.id === this.pokemons[0].id); // Find the pokemon
      this.pokemons[0].wins += 1 // Add win 
      pokemon!.wins = this.pokemons[0].wins
      this.pokemonService.setWinner(this.user?.pokemon) // Set winner
    .subscribe({
      next: (user: User) => {
        // Check complications
        if(this.pokemons[0].id == null || this.pokemons[1].id == null){
          alert("Pokemon id null")
        }
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR: " + error.message);
      }
    })
    } 
    // Same logic here for pokemon 2
    else if(this.pk2.isAlive){
      this.battleText = `${this.pk2.name} win`
      this.pokemons[1].wins += 1
      let pokemon = this.userService.user?.pokemon.find(x => x.id === this.pokemons[1].id);
      console.log(pokemon)
      pokemon!.wins = this.pokemons[1].wins
      this.pokemonService.setWinner(this.user?.pokemon)
    .subscribe({
      next: (user: User) => {
        if(this.pokemons[0].id == null || this.pokemons[1].id == null){
          alert("Pokemon id null")
        }
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR: " + error.message);
      }
    })
    }
    
  }
}
