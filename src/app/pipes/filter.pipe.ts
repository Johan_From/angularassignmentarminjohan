import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  // Pipe for filtering
  transform(value: any, filterString: string) {
    if(value.length === 0 || filterString === ""){
      return value;
    }

    const pokemons = [];
    //Foreach pokemon in value, check if pokemons includes searchstring
    for(const pokemon of value){
      if(pokemon['name'].toLowerCase().includes(filterString.toLowerCase())){
        // Add pokemon to pokemons array
        pokemons.push(pokemon)
      }
    }
    return pokemons
  }

}
