// Interface for pokemon object
export interface Pokemon {
    id: string;
    name: string;
    image: string;
    baseExperience: number; // Check om det är number
    element: string,
    ability1: string,
    ability2: string,
    health: number,
    isAlive: boolean,
    attack: number, 
    wins: number
}