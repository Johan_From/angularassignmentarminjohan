import { Pokemon } from "./pokemon.model";

// Interface for user object that matches the api
export interface User {
    id: number;
    username: string;
    pokemon: Pokemon[];
}