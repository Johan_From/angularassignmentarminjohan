<h1>Pokémon Trainer</h1>
<hr><p>An Angular application that uses an API for fetching Pokémons and an other API for fecthing users. The functionality that the app provides is that you can see different pokémons and decide which one to catch and see them in your own user profile. </p><h2>General Information</h2>
<hr><ul>
<li>To make this app, the vscode extension live share was being used so the two authors could code together.</li>
<li>The app was built with the Angular framework and utilizes Tailwind CSS for styling.</li>
<li>The app's purpose is to utilize the technologies below in a functional matter.</li>
<h2>Usages</h2>

This app uses the ```environment.ts``` and ```environment.prod.ts``` files for storing sensitive information. Create three variables as described in the two files that you find in the src folder.

```
ANGULAR_APP_API_KEY="<your key>"
ANGULAR_APP_API_URL="<your url>"
POKEMON_API_URL="https://pokeapi.co/api/v2/pokemon?limit=151"
```

</ul><h2>Technologies Used</h2>
<hr><ul>
<li>TypeScript</li>
</ul><ul>
<li>Angular (Angular CLI)</li>
</ul><ul>
<li>Tailwind CSS</li>
</ul><ul>
</ul><ul>
<li>Heroku (Heroku CLI)</li>
</ul><h2>Setup</h2>
<hr><p>To get the app started follow the steps below:</p>

```
$ git clone https://gitlab.com/Johan_From/angularassignmentarminjohan.git
$ cd angularassignmentarminjohan
$ npm install
$ ng serve
```

NOTE: For running the ```ng``` command, make sure you have the Angular CLI downloaded. If not run the command below.

```
$ npm install -g @angular/cli
```

<h2>Collaborators</h2>
<ul>
<li><a href="https://gitlab.com/Johan_From" target="_blank">Johan From</a></li>
<li><a href="https://gitlab.com/ArminExperis" target="_blank">Armin Ljajic</a></li>
</ul>
